#include<stdio.h>

int input()
{
    int num;

    printf("Enter a number : ");
    scanf("%d",&num);  

    return num;
}

int compute(int a)
{
    int sum=0;

    for(;a>0;a=a/10)
    {
        sum=sum+a%10;
    }
    return sum;
}

void output(int sum)
{
    printf("Sum is : %d",sum);
}

int main()
{
    int num,sum;
    num=input();
    sum=compute(num);
    output(sum);
    return 0;
}
