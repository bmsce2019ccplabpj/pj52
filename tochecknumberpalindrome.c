#include<stdio.h>

int input()
{
    int a;
    printf("Enter the number");
    scanf("%d",&a);
    return a;
}

int compute(int a)
{
    int c,reverse=0;
    for(a;a>0;a=a/10)
    {
        reverse=reverse*10;
        reverse=reverse+a%10;
    }
    return reverse;
}

void output(int a,int b)
{
    if(a==b)
        printf("It is a palindrome");
    else printf("It is not a palindrome");
}

int main()
{
    int a,b;
    a=input();
    b=compute(a);
    output(a,b);
    return 0;
}
