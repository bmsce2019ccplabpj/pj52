#include<stdio.h>

int input()
{
    int a;
    printf("Enter the number");
    scanf("%d",&a);
    return a;
}

int compute(int a)
{
    int b,reverse=0;
    for(a;a>0;a=a/10)
    {
        b=a%10;
        reverse=reverse*10;
        reverse=reverse+b;
    }
    return reverse;
}

void output(int a)
{
  printf("reverse is %d",a);  
}

int main()
{
    int a,b;
    a=input();
    b=compute(a);
    output(b);
    return 0;
}