#include <stdio.h>

void input(int *a,int *b)
{
    printf("Enter the time in hours and minutes/n");
    scanf("%d%d",&*a,&*b);
}

int compute(int a,int b)
{
    int result;
    result=a*60+b;
    return result;
}

void output(int c)
{
    printf("time in minutes is %d/n",c);
}

int main()
{
   int a,b,result;
   input(&a,&b);
   result=compute(a,b);
   output(result);
   return 0;
}