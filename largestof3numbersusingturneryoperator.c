#include<stdio.h>

void input(int *a,int *b,int *c)
{
    printf("Enter three numbers\n");
    scanf("%d%d%d",&*a,&*b,&*c);
}

int compute(int a,int b,int c)
{
    int result;
    result=(a>=b && a>=c)?a:((b>=a && b>=c)?b:c);
    return result;
}

void output(int a)
{
    printf("The largest number is %d\n",a);
}

int main()
{
    int a,b,c,result;
    input(&a,&b,&c);
    result=compute(a,b,c);
    output(result);
    return 0;
}