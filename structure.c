#include<stdio.h>

typedef struct  
{
    int id;
    char name[20];
    double salary;
    char doj[10];
}employee;

employee input()
{
    employee e1;
    printf("Enter the following Employee details\n");

    printf("\nEmplyee ID : ");
    scanf("%d",&e1.id);

    printf("Employee Name : ");
    scanf("%s",e1.name);

    printf("Employee Salary : ");
    scanf("%lf",&e1.salary);

    printf("Employee Date of joining : ");
    scanf("%s",e1.doj);
    return e1;
}

void output(employee e1)
{
    printf("\nEmployee details are\n");

    printf("\nEmplyee ID :%d",e1.id);

    printf("\nEmployee Name :%s",e1.name);

    printf("\nEmployee Salary :%.2lf",e1.salary);

    printf("\nEmployee Date of joining :%s",e1.doj);
}

int main()
{
    employee e1;

    e1=input();
    output(e1);

    return 0;
} 