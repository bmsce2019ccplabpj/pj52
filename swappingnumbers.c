#include<stdio.h>

void input(int *a,int *b)
{
    printf("Enter 2 numbers to be swaped : ");
    scanf("%d%d",&*a,&*b);
}

void compute(int *a,int *b)
{
    int t;
    t=*a;
    *a=*b;
    *b=t;
}

void output1(int a,int b)
{
    printf("\nNumbers before swapping a:%d, b:%d",a,b);
}

void output2(int a,int b)
{
    printf("\n\nNumbers after swapping a:%d, b:%d\n",a,b); 
}

int main()
{
    int a,b;

    input(&a,&b);
    output1(a,b);
    compute(&a,&b);
    output2(a,b);
    
    return 0;
}