#include<stdio.h>

void input1()
{
    printf("enter the numbers\n Enter 0 to stop\n");
}

int input2()
{
    int a;
    scanf("%d",&a);
    return a;
}

float result()
{
    int a,i=0,b=0;
    float average;
    input1();
    
     do
    {
        a=input2();
        if(a>0)
        {
            b+=a;
            i++;
        }
    } while(a!=0);
    average=(float)b/i;
    
   return average;
}

void output(float a)
{
   printf("Average of positive numbers is %f",a);
}


int main()
{
  float a;
  a=result();
  output(a);
  return 0;
}
