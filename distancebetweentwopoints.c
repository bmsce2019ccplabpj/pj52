#include<stdio.h>
#include<math.h>

void input(int *a,int *b,int *c,int *d)
{
    printf("Enter ordinates of first point/n");
    scanf("%d%d",&*a,&*b);
    printf("Enter ordinates of second point/n");
    scanf("%d%d",&*c,&*d);
}

float compute(int a,int b,int c,int d)
{
    float result;
    result=sqrt(pow(c-a,2)+pow(d-b,2));
    return result;
}

void output(float a)
{
    printf("Distance between two points is %f",a);
}

int main()
{
    int a,b,c,d;
    float result;
    input(&a,&b,&c,&d);
    result=compute(a,b,c,d);
    output(result);
    return 0;
}
  