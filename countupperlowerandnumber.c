#include<stdio.h>

void input1()
{
    printf("Enter the characters\n Enter * to stop\n");
}

char input2()
{
    char a;
    scanf("%c",&a);
    return a;
}

void compute(char a,int *u,int *l,int *n)
{
    
    if(a>='A' && a<='Z')
    (*u)++;
    
    else if(a>='a' && a<='z')
    (*l)++;
    
    else if (a>='0' && a<='9')
    (*n)++; 
 }

void output(int u,int l,int n)
{
    printf("Number of uppercase letters are %d\n",u);
    printf("Number of lowercase letters are %d\n",l);
    printf("Number of integers entered are %d\n",n);
}
 
int main()
{
    int u=0,l=0,n=0;
    char c;
    input1();
    scanf(" %c",&c);
    while(c!='*') {
        compute(c,&u,&l,&n);
        c = input2();
    }
    output(u,l,n);
    return 0;
}
