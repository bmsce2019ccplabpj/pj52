#include<stdio.h>

int input()
{
    int a;
    printf("enter the number");
    scanf("%d",&a);
    return a;
}

int result(int a)
{
    int b;
    if(a%2==0)
        b=0;
    else b=1;

    return b;
}

void output(int a,int b)
{
    switch(b)
    {
        case 0:
            printf("the number %d is even",a);
            break;

        case 1:
            printf("the number %d is odd",a);
            break;

    }
}

int main ()
{
    int a,b;

    a=input();
    b=result(a);
    output(a,b);

    return 0;
}