#include<stdio.h>

void input(char a[100],char b[100])
{
    printf("Enter the first string : ");
    gets(a);
    
    printf("\nEnter the second string : ");
    gets(b);
}

void compute(char a[100],char b[100],char c[100])
{
    int i=0,j=0;
    
    while(a[i]!='\0')
    {
        c[j]=a[i];
        i++;
        j++;
    }
    
    i=0;
    
    while(b[i]!='\0')
    {
        c[j]=b[i];
        i++;
        j++;
    }
    
    c[j]='\0';
}

void output(char c[100])
{
    printf("\nString after concatenation : ");
    puts(c);
}

int main()
{
    char a[100],b[100],c[100];
    input(a,b);
    compute(a,b,c);
    output(c);
    return 0;
}
