#include<stdio.h>

void input(int marks[5][3])
{
    int i,j;
    for(i=0;i<5;i++)
    {
        printf("Enter marks obtained by student %d\n",i);

        for(j=0;j<3;j++)
        {
            printf("Marks[%d][%d] = ",i,j);
            scanf("%d",&marks[i][j]);
        }
    }   
}

void compute(int marks[5][3],int *a,int *b,int *c)
{
    int i,j,highest_marks;

    for(j=0;j<3;j++)
    {
        highest_marks=marks[0][i];
        for(i=0;i<5;i++)
        {
            if(marks[i][j]>highest_marks)
                highest_marks=marks[i][j];
        }
        if(j==0)
            *a=highest_marks;
        else if(j==1)
            *b=highest_marks;
        else *c=highest_marks;
    }
}

void output(int a,int b,int c)
{
    printf("Highest marks in course 1 : %d \n Highest marks in course 2 : %d \n Highest marks in course 3 : %d \n",a,b,c);
}

int main()
{
    int marks[5][3],a=0,b=0,c=0;
    input(marks);
    compute(marks,&a,&b,&c);
    output(a,b,c);
    return 0;
}