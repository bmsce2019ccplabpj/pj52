#include<stdio.h>

int input()
{
    int a;
    printf("Enter the number you want to search\n");
    scanf("%d",&a);
    return a;
}

int compute(int key,int n,int array[],int *mid)
{
    int beg=0,end=n-1,flag;
    while(beg<=end)
    {
        *mid=(beg+end)/2;

        if(array[*mid]==key)
        {
            flag=1;
            break;
        }
        else if(array[*mid]<key)
            beg=*mid+1;

        else end=end+1;
    }
    return flag;
}

void output(int a,int b,int c)
{
    if(b==1)
    printf("The element %d is found at %d",a,c+1);
    else
        printf("The element %d is not found",a);
}

int main()
{
    int a,b,mid,n=10,array[10]={1,2,3,4,5,6,7,8,9,10};
    a=input();
    b=compute(a,n,array,&mid);
    output(a,b,mid);
}

