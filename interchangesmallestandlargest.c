#include<stdio.h>

void input(int vinay[10],int *size)
{
    int i;
    
    printf("Enter size of the array : ");
    scanf("%d",&*size);
    
    printf("Enter the elements of the array\n");
    
    for(i=0;i<*size;i++)
    {
        scanf("%d",&vinay[i]);
    }
}

void compute(int vinay[10],int size,int *largest,int *smallest,int *flag1,int *flag2)
{
    int i;
    
    *largest=vinay[size-1];
    *smallest=vinay[0];
    
    for(i=0;i<size;i++)
    {
        if(vinay[i]>=*largest)
        {
         *largest=vinay[i];
         *flag1=i;      
        }
        
        else if(vinay[i]<=*smallest)
        {
         *smallest=vinay[i];
         *flag2=i;   
        }
    }
}

void output(int vinay[10],int size,int largest,int smallest,int flag1,int flag2)
{
    int i;
    
    printf("\nElements of the array before interchange\n");
    
    for(i=0;i<size;i++)
    {
        printf("%d\t",vinay[i]);
    }
    
    printf("\n\n");
    
    printf("Largest number %d is at position %d\n",largest,flag1);
    printf("Smallest number %d is at position %d\n",smallest,flag2);
    
    vinay[flag1]=smallest;
    vinay[flag2]=largest;
    
    printf("\nElements after interchange is\n");
    
    for(i=0;i<size;i++)
    {
        printf("%d\t",vinay[i]);
    }
}

int main()
{
    int vinay[10],size,largest,smallest,flag1=0,flag2=0;
    
    input(vinay,&size);
    compute(vinay,size,&largest,&smallest,&flag1,&flag2);
    output(vinay,size,largest,smallest,flag1,flag2);
    
    return 0;
}