#include<stdio.h>

void input1()
{
    printf("Enter the numbers\n Enter 0 to stop\n");
}

int input2()
{
    int a;
    scanf("%d",&a);
    return a;
}

void compute(int *c,int *p)
{
    int a,i,flag=0;
    *c=0,*p=0;

    do
    {
        a=input2();

        for(i=2;i<=a-1;i++)
        {
            if(a%i==0)
            {
                flag=1;
                break;
            }
        }
        if(flag==1)
            *c++;
        else *p++;

    } while(a!=0);
}

void output(int a,int b)
{
    printf("Number of composite numbers are %d\n",a);
    printf("Number of prime numbers are %d\n",b);
}

int main()
{
    int a,b;
    input1();
    compute(&a,&b);
    output(a,b);
    return 0;
}
